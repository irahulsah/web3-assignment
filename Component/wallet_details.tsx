import { MouseEventHandler } from 'react'

interface WalletDetailsProps {
  chainId: number | undefined
  balance: string | undefined
  address: string | undefined
  disconnectWallet: MouseEventHandler<HTMLButtonElement>
}
const WalletDetails = (props: WalletDetailsProps) => {
  return (
    <div
      className="relative w-full max-w-2xl max-h-full"
      style={{ width: '400px' }}
    >
      <div className="flex flex-row p-4 space-x-2 justify-between">
        <div>Key</div>
        <div>Value</div>
      </div>
      <div className="flex flex-row p-4 space-x-2 justify-between border-t border-gray-200 rounded-b dark:border-gray-600">
        <div className="">Account</div>
        <p className="">{props.address}</p>
      </div>
      <div className="flex flex-row p-4 space-x-2 justify-between border-t border-gray-200 rounded-b dark:border-gray-600">
        <div>Chain ID</div>
        <div>{props.chainId}</div>
      </div>
      <div className="flex flex-row p-4 space-x-2 justify-between border-t border-gray-200 rounded-b dark:border-gray-600">
        <div>Balance</div>
        <div>{props.balance}</div>
      </div>
      <p className="font-light p-4 space-x-2 flex justify-center">
        Wallet Details
      </p>
      <button
        className="bg-red-500 hover:bg-red-400 text-white font-bold py-2 px-4 rounded w-full"
        onClick={props.disconnectWallet}
        type='button'
      >
        Disconnect
      </button>
    </div>
  )
}

export default WalletDetails
