import { useState, useEffect, useMemo, MouseEventHandler } from 'react'
import { useWeb3React } from '@web3-react/core'
import BigNumber from 'bignumber.js'
import { web3BNToFloatString } from '@/lib/connect-wallet/utils'
import { injected } from '@/lib/connect-wallet/connector'
import WalletDetails from './wallet_details'

interface WalletPopupprops {
  closeModal: MouseEventHandler<HTMLButtonElement>
}

export default function WalletPopup(props: WalletPopupprops) {
  const { active, account, library, activate, connector, deactivate, error, chainId } =
    useWeb3React()

  const [balance, setBalance] = useState<string>()

  async function connectWallet() {
    try {
      await activate(injected)
      localStorage.setItem('isWalletConnected', 'true')
    } catch (err) {
      alert(error)
    }
  }


  function disconnectWallet() {
    try {
      deactivate()
      localStorage.setItem('isWalletConnected', 'false')
    } catch (error) {
      alert(error)
    }
  }

  // initialize wallet connection even after reload.
  useEffect(() => {
    const connectWalletOnPageLoad = async () => {
      if (localStorage?.getItem('isWalletConnected') === 'true') {
        try {
          await activate(injected)
          localStorage.setItem('isWalletConnected', 'true')
        } catch (error) {
          alert(error)
        }
      }
    }
    connectWalletOnPageLoad()
  }, [])

  const getBNBBalance = async () => {
    try {
      const provider = await library.eth.getBalance(account)
      const pow = new BigNumber('10').pow(new BigNumber(18))
      const balance = web3BNToFloatString(
        provider,
        pow,
        2,
        BigNumber.ROUND_DOWN
      )
      setBalance(balance)
    } catch (err) {
      console.error(err)
    }
  }

  //  shorten account address
  const address = useMemo(() => {
    if (account) {
      console.log(account.slice(-1, 5))
      getBNBBalance()
      return account.slice(0, 4) + '...' + account.slice(-4)
    }
  }, [account])

  const WalletDetailsProps = {
    chainId,
    address,
    balance,
    disconnectWallet,
  }

  return (
    <div className="relative w-400 max-w-2xl max-h-full py-10">
      <div>
        {active ? (
          <WalletDetails {...WalletDetailsProps} />
        ) : (
          <>
            {/* handling error , if metamask is not installed then
             show to let them install first to continue*/}
            {error ?
              <div className="text-red-500 font-sm mb-5">
                Please download metamask extension to continue.
              </div> :
              <div className="text-red-500 font-lg">
                Wallet not connected. Please click the 'Connect Now' button below.
              </div>
            }
            <div className="flex justify-between mt-10">
              <button
                className="bg-[#026080] hover:bg-[#1791BA] text-white font-bold py-2 px-4 rounded w-full mx-2"
                type="button"
                onClick={connectWallet}
              >
                {active ? 'Disconnect' : 'Connect'}
              </button>
              <button
                className="bg-[#E5ECEE] hover:bg-grey-200 text-black font-bold py-2 px-4 rounded w-full"
                type="button"
                onClick={props.closeModal}
              >
                Cancel
              </button>
            </div>
          </>
        )}
      </div>
    </div>
  )
}
