import { useState } from 'react'
import Image from 'next/image'
import WalletPopup from '@/Component/wallet_popup'
export default function Home() {
  // open or close modal popup
  const [open, setOpen] = useState<boolean>(false)
  const [nep, setNep] = useState<number>()
  const [busd, setBusd] = useState<number>()

  const handleNepChange = (event: { target: { value: any } }) => {
    const nepValue = event.target.value
    const busdValue = nepValue * 3
    setNep(nepValue)
    setBusd(busdValue)
  }

  const handleBusdChange = (event: { target: { value: any } }) => {
    const busdValue = event.target.value
    const nepValue = busdValue / 3
    setBusd(busdValue)
    setNep(nepValue)
  }

  const closeModal = () => setOpen(false)

  return (
    <div className="bg-purple-200 h-screen">
      <div className="flex items-center justify-center py-20">
        <Image width={200} height={200} src={'/logo.svg'} alt={'logo.svg'} />
      </div>
      <div className="flex items-center justify-center py-10  h-[70vh]">
        <div className="w-full max-w-sm">
          <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="flex justify-start mb-6 text-2xl font-bold">
              Crypto Converter
            </div>
            <div className="mb-4">
              <label
                className="block text-gray-700 text-sm font-light mb-2"
                htmlFor="username"
              >
                NEP
              </label>
              <input
                value={nep}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                id="username"
                type="text"
                placeholder="0.00"
                onChange={handleNepChange}
              />
            </div>
            <div className="flex justify-center items-center" color="#e0dcdc">
              <Image
                width={30}
                height={30}
                src={'/retweet-solid.svg'}
                alt={'retweet-solid.svg'}
              />
            </div>
            <div className="mb-6">
              <label
                className="block text-gray-700 text-sm font-light mb-2"
                htmlFor="password"
              >
                BSUD
              </label>
              <input
                value={busd}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="password"
                type="text"
                placeholder="0.00"
                onChange={handleBusdChange}
              />
            </div>
            <div className="flex justify-center align-items">
              <button
                className="text-blue-600 mb-6 text-md font-bold"
                onClick={() => setOpen((prev) => !prev)}
                type="button"
              >
                Check Wallet Details
              </button>
            </div>
          </form>
          <p className="text-center text-gray-500 text-xs">
            &copy;2023 NEPTUNE MUTUAL. All rights reserved.
          </p>
        </div>
      </div>
      {open && (
        <div className="modal-overlay">
          <div className="modal">
            <div className="flex flex-row justify-between">
              <div className="text-xl font-bold">Wallet Details</div>
              <div className="flex justify-end" onClick={closeModal}>
                <Image
                  width={20}
                  height={20}
                  src={'/close.svg'}
                  alt="close.svg"
                />
              </div>
            </div>
           
    <div className="relative w-full max-w-2xl max-h-full">
        <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
           <WalletPopup closeModal={closeModal} />
        </div>
        
    </div>

          </div>
        </div>
      )}
    </div>
  )
}
